# Changes to SMTK Plugins

SMTK's plugin system now facilitates the registration of new managers in addition to registering
things to existing managers.
