+ Many Qt widgets in SMTK's GUI extensions are now assigned object names
  so that testing can be more reliable. Note that if you have existing
  XML tests, these may need to be updated to reflect the new names.
  The new names include attribute names and item names, which also makes
  the test XML easier to understand.
